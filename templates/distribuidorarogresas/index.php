<?php

/* ************************************************************************************************************************

DISTRIBUIDORA ROGRE S.A.S

File:			index.php
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2016

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );
$pageclass = $app->getParams( 'com_content' );
$class = $pageclass->get( 'pageclass_sfx' );

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_about_us = $this->countModules( 'about_us' );
$show_banner = $this->countModules( 'banner' );
$show_bottom = $this->countModules( 'bottom' );
$show_contact_us = $this->countModules( 'contact_us' );
$show_product = $this->countModules( 'product' );
$show_tip = $this->countModules( 'tip' );
$show_top = $this->countModules( 'top' );

// Params

?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="<?php echo $this->language; ?>"><![endif]-->
<html class="no-js" lang="<?php echo $this->language; ?>">
	<head>
		<jdoc:include type="head" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="-va0MHyeEobIVMS7DMRJeFp-2YBv7RlXoLXJAtd8V7U">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<link href="<?php echo $path; ?>js/foundation/css/normalize.css" rel="stylesheet">
		<link href="<?php echo $path; ?>js/foundation/css/foundation.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>css/animate.css/animate.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen">
		<link href="<?php echo $path; ?>css/template.css" rel="stylesheet" type="text/css">
		<script src="<?php echo $path; ?>js/foundation/js/vendor/modernizr.js"></script>
		<!-- Begin Google Analytics -->
		<script>

			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-74928858-1', 'auto');
			ga('send', 'pageview');

		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<?php if ( $show_top ) : ?>
		<!-- Begin Menu -->
			<section id="menu" class="menu show-for-small-only">
				<div class="row">
					<div class="small-12 columns text-center logo">
						<a href="#banner" id="link_banner">
							<img src="<?php echo $path; ?>images/logo.png" title="<?php echo $app->getCfg( 'sitename' ); ?>" alt="<?php echo $app->getCfg( 'sitename' ); ?>">
						</a>
					</div>
				</div>
				<div class="row">
					<nav class="top-bar" data-topbar role="navigation">
						<ul class="title-area">
							<li class="name"></li>
							<li class="toggle-topbar menu-icon"><a href="#"><span>Menú</span></a></li>
						</ul>
						<section class="top-bar-section">
							<jdoc:include type="modules" name="menu" style="xhtml" />
						</section>
					</nav>
				</div>
			</section>
		<!-- End Menu -->
		<!-- Begin Top -->
			<section id="top" class="top">
				<div class="row">
					<jdoc:include type="modules" name="top" style="xhtml" />
				</div>
			</section>
		<!-- End Top -->
		<?php endif; ?>
		<?php if ( $show_banner ) : ?>
		<!-- Begin Banner -->
			<section id="banner" class="banner">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="banner" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Banner -->
		<?php endif; ?>
		<?php if ( $show_about_us ) : ?>
		<!-- Begin About Us -->
			<section id="about_us" class="about_us">
				<jdoc:include type="modules" name="about_us" style="xhtml" />
			</section>
		<!-- End About Us -->
		<?php else : ?>
		<!-- Begin Component -->
			<section id="component" class="component">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="component" />
					</div>
				</div>
			</section>
		<!-- End Component -->
		<?php endif; ?>
		<?php if ( $show_product ) : ?>
		<!-- Begin Product -->
			<section id="product" class="product">
				<jdoc:include type="modules" name="product" style="xhtml" />
			</section>
		<!-- End Product -->
		<?php endif; ?>
		<?php if ( $show_tip ) : ?>
		<!-- Begin Tip -->
			<section id="tip" class="tip">
				<jdoc:include type="modules" name="tip" style="xhtml" />
			</section>
		<!-- End Tip -->
		<?php endif; ?>
		<?php if ( $show_contact_us ) : ?>
		<!-- Begin Contact Us -->
			<section id="contact_us" class="contact_us">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="contact_us" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Contact Us -->
		<?php endif; ?>
		<?php if ( $show_bottom ) : ?>
		<!-- Begin Bottom -->
			<section id="bottom" class="bottom">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="bottom" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Bottom -->
		<!-- Begin Copyright -->
			<div class="copyright_wrap">
				<div class="copyright">
					&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>"><?php echo $app->getCfg( 'sitename' ); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapolazul</a>.
				</div>
			</div>
		<!-- End Copyright -->
		<?php endif; ?>
		<!-- Begin Main Scripts -->
			<script src="<?php echo $path; ?>js/foundation/js/vendor/jquery.js"></script>
			<script src="<?php echo $path; ?>js/foundation/js/foundation.min.js"></script>
			<script src="<?php echo $path; ?>js/jquery/animatescroll/animatescroll.min.js" type="text/javascript"></script>
			<script src="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.pack.js?v=2.1.5" type="text/javascript"></script>
			<script>
				$(document).foundation();
			</script>
			<script src="<?php echo $path; ?>js/jquery/scripts.js" type="text/javascript"></script>
		<!-- End Main Scripts -->
	</body>
</html>