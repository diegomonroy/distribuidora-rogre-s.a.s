// JavaScript Document

/* ************************************************************************************************************************

DISTRIBUIDORA ROGRE S.A.S

File:			scripts.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2016

************************************************************************************************************************ */

jQuery.noConflict();

/* Add ID */

function addID( element, id ) {
	element = jQuery( element );
	element.attr( 'id', id );
}

/* Animation on Click */

function animationClick( element, section, animation, speed, space ) {
	element = jQuery( element );
	element.click(
		function() {
			jQuery( section ).animatescroll({
				easing: animation,
				scrollSpeed: speed,
				padding: space
			});
		}
	);
}

jQuery(document).ready(function() {

	// AnimateScroll

	addID('li.item-115 a', 'link_banner');
	addID('li.item-128 a', 'link_banner');
	addID('li.item-129 a', 'link_banner');
	animationClick('#link_banner', '#banner', 'easeOutBounce', 2000, 0);
	addID('li.item-113 a', 'link_about_us');
	addID('li.item-124 a', 'link_about_us');
	addID('li.item-125 a', 'link_about_us');
	animationClick('#link_about_us', '#about_us', 'easeOutBounce', 2000, 120);
	addID('li.item-114 a', 'link_product');
	addID('li.item-126 a', 'link_product');
	addID('li.item-127 a', 'link_product');
	animationClick('#link_product', '#product', 'easeOutBounce', 2000, 120);
	addID('li.item-116 a', 'link_tip');
	addID('li.item-130 a', 'link_tip');
	addID('li.item-131 a', 'link_tip');
	animationClick('#link_tip', '#tip', 'easeOutBounce', 2000, 120);
	addID('li.item-117 a', 'link_contact_us');
	addID('li.item-132 a', 'link_contact_us');
	addID('li.item-133 a', 'link_contact_us');
	animationClick('#link_contact_us', '#contact_us', 'easeOutBounce', 2000, 120);

	// fancyBox

	jQuery( '.fancybox' ).fancybox();

});